package utilities

import (
	"encoding/json"
	"gitlab.com/Chomsky49/signos-be/pkg/context"
	"net/http"
	"reflect"
	"slices"
)

type Masking struct {
	ParameterData   []string `json:"parameter_data"`
	ParameterHeader []string `json:"parameter_header"`
}

func ConvertToFieldMask(config []string, s any) interface{} {

	valueOf := reflect.ValueOf(s)
	if valueOf.Kind() == reflect.Ptr {
		valueOf = valueOf.Elem()
	}

	if valueOf.Kind() == reflect.Slice || valueOf.Kind() == reflect.Array {
		// array
		return handlingArray(config, s, valueOf)
	} else {
		if valueOf.Kind() == reflect.String {
			return "***"
		} else {
			myMap := make(map[string]interface{})
			byteArr, err := json.Marshal(s)
			if err == nil {
				err = json.Unmarshal(byteArr, &myMap)
				if err == nil {
					maskingItems(config, myMap)
					return myMap
				}
			}
			return s
		}
	}
}

func handlingArray(config []string, originalValue any, valueOf reflect.Value) any {
	_, ok := valueOf.Interface().([]string)
	if !ok {
		items := convertAnyToArrayOfInterface(valueOf.Interface())
		for _, data := range items {
			maskingItems(config, data)
		}
		if items == nil {
			return originalValue
		}
		return items
	} else {
		// Masking For Array String

		//var items []string
		//for i := 0; i < valueOf.Len(); i++ {
		//	items = append(items, "***")
		//}
		return originalValue
	}
}

func handlingObject(config []string, originalValue any, valueOf reflect.Value) any {
	if valueOf.Kind() == reflect.String {
		return "***"
	} else {
		myMap := make(map[string]interface{})
		byteArr, err := json.Marshal(originalValue)
		if err == nil {
			err = json.Unmarshal(byteArr, &myMap)
			if err == nil {
				maskingItems(config, myMap)
				return myMap
			}
		}
		return originalValue
	}
}

func checkFieldMask(config []string, s interface{}) interface{} {

	valueOf := reflect.ValueOf(s)

	if valueOf.Kind() == reflect.Ptr {
		valueOf = valueOf.Elem()
	}

	if valueOf.Kind() != reflect.Struct && valueOf.Kind() != reflect.Map {
		return nil
	}
	if valueOf.CanSet() {
		for _, data := range config {

			fieldMask := valueOf.FieldByName(data)
			if fieldMask.IsValid() && fieldMask.Kind() == reflect.String {
				fieldMask.SetString("***")
			}
		}
	}
	return valueOf.Interface()
}

func CheckFieldMaskHeader(config []string, ctx *context.UlfsaarContext) {
	for _, data := range config {
		ctx.Header.(http.Header).Set(data, "***")
	}
}

func maskingItems(config []string, item map[string]interface{}) {
	for key, _ := range item {
		if reflect.Struct == reflect.TypeOf(item[key]).Kind() || reflect.Map == reflect.TypeOf(item[key]).Kind() {
			maskingItems(config, item[key].(map[string]interface{}))
		} else if reflect.Array == reflect.TypeOf(item[key]).Kind() || reflect.Slice == reflect.TypeOf(item[key]).Kind() {
			items := convertAnyToArrayOfInterface(item[key])
			if items != nil {
				item[key] = items
				for _, data := range item[key].([]map[string]interface{}) {
					maskingItems(config, data)
				}
			}
		} else {
			exist := slices.Contains(config, key)
			if exist {
				item[key] = "***"
			}
		}
	}
}

func convertAnyToArrayOfInterface(a any) []map[string]interface{} {
	var res []map[string]interface{}
	value := unPackArray(a)
	for i := 0; i < len(value); i++ {
		item := value[i]
		t := reflect.TypeOf(item).Kind()
		if t == reflect.Struct || t == reflect.Map {
			var inInterface map[string]interface{}
			inRec, _ := json.Marshal(item)
			err := json.Unmarshal(inRec, &inInterface)
			if err != nil {
				continue
			}
			res = append(res, inInterface)
		}
	}
	return res
}

func unPackArray(a any) []any {
	v := reflect.ValueOf(a)
	data := make([]any, v.Len())
	for i := 0; i < v.Len(); i++ {
		data[i] = v.Index(i).Interface()
	}
	return data
}
