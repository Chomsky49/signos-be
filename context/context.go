package context

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/Chomsky49/signos-be/errors"
	"gitlab.com/Chomsky49/signos-be/session"
)

type (
	UlfsaarContext struct {
		echo.Context
		Session *session.Session
	}

	Success struct {
		Code    string      `json:"code"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	Failed struct {
		Code    string `json:"code"`
		Message string `json:"message"`
		Error   string `json:"error"`
	}

	FailedWithData struct {
		Code      string      `json:"code"`
		Message   string      `json:"message"`
		Error     string      `json:"error"`
		ErrorData interface{} `json:"error_data"`
	}
)

func (sc *UlfsaarContext) Success(data interface{}) error {
	return sc.JSON(200, Success{
		Code:    "200",
		Message: "success",
		Data:    data,
	})
}

func (sc *UlfsaarContext) SuccessWithMeta(data, meta interface{}) error {
	return sc.JSON(200, Success{
		Code:    "200",
		Message: "success",
		Data:    data,
	})
}

func (sc *UlfsaarContext) Fail(err error) error {
	var (
		ed = errors.ExtractError(err)
	)

	return sc.JSON(ed.HttpCode, Failed{
		Code:    ed.Code,
		Message: "failed",
		Error:   ed.Message,
	})
}

func (sc *UlfsaarContext) FailWithData(err error, data interface{}) error {
	var (
		ed = errors.ExtractError(err)
	)

	return sc.JSON(ed.HttpCode, FailedWithData{
		Code:      ed.Code,
		Message:   "failed",
		Error:     ed.Message,
		ErrorData: data,
	})
}

func NewEmptyUlfsaarContext(parent echo.Context) *UlfsaarContext {
	return &UlfsaarContext{parent, nil}
}

func NewUlfsaarContext(parent echo.Context) (*UlfsaarContext, error) {
	pctx, ok := parent.(*UlfsaarContext)
	if !ok {
		return nil, errors.ErrSession
	}
	if pctx.Session == nil {
		return nil, errors.ErrSession
	}
	return pctx, nil
}
