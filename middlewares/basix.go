package middlewares

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/Chomsky49/signos-be/errors"
)

type (
	BasicAuthorizationMiddleware interface {
		BasicAuthenticate(next echo.HandlerFunc) echo.HandlerFunc
	}

	basicAuthorizationMiddleware struct {
		username, password string
	}
)

func (i *basicAuthorizationMiddleware) BasicAuthenticate(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if i.skipper(ctx) {
			return next(ctx)
		}

		username, password, ok := ctx.Request().BasicAuth()
		if !ok {
			return errors.ErrSession
		}

		isValid := (username == i.username) && (password == i.password)
		if !isValid {
			return errors.ErrSession
		}

		return next(ctx)
	}
}

func (i *basicAuthorizationMiddleware) skipper(c echo.Context) (skip bool) {
	url := c.Request().URL.String()
	if url == "/" {
		skip = true
		return
	}

	return
}

func NewBasicAuthorizationMiddleware(username, password string) (BasicAuthorizationMiddleware, error) {
	return &basicAuthorizationMiddleware{username, password}, nil
}
