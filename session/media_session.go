package session

import (
	"github.com/go-playground/validator/v10"
	"gitlab.com/Chomsky49/signos-be/crypto"
	"gitlab.com/Chomsky49/signos-be/errors"
)

type (
	MediaSession struct {
		Role      string   `json:"user_type" validate:"required"`
		Directory string   `json:"directory" validate:"required"`
		Filenames []string `json:"filenames" validate:"required"`
		UserID    string   `json:"user_id" validate:"required"`
	}
)

func (ss *MediaSession) Encrypt(cr crypto.Crypto) (string, error) {
	enc, _ := cr.Encrypt(ss)

	return string(enc), nil
}

func (ss *MediaSession) Valid() error {
	return nil
}

func NewMediaSession(cr crypto.Crypto, session string) (*MediaSession, error) {
	var (
		ss       = &MediaSession{}
		dec, err = cr.Decrypt(ss, session)
	)

	if err != nil {
		return nil, errors.ErrSession.WithUnderlyingErrors(err)
	}

	if err := validator.New().Struct(dec); err != nil {
		return nil, errors.ErrSession.WithUnderlyingErrors(err)
	}

	return ss, nil
}
