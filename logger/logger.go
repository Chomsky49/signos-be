package logger

import (
	"context"
	"github.com/sirupsen/logrus"
	"gopkg.in/natefinch/lumberjack.v2"
)

type (
	Logger interface {
		Info(...interface{})
		Infof(string, ...interface{})
		Debug(...interface{})
		Debugf(string, ...interface{})
		Error(...interface{})
		Errorf(string, ...interface{})
		Warning(...interface{})
		Warningf(string, ...interface{})
		Fatal(...interface{})
		Fatalf(string, ...interface{})
		Print(...interface{})
		Printf(string, ...interface{})
		Println(...interface{})
		Instance() interface{}

		DebugWithCtx(context.Context, string, ...Field)
		DebugfWithCtx(context.Context, string, ...interface{})
		InfoWithCtx(context.Context, string, ...Field)
		InfofWithCtx(context.Context, string, ...interface{})
		WarnWithCtx(context.Context, string, ...Field)
		WarnfWithCtx(context.Context, string, ...interface{})
		ErrorWithCtx(context.Context, string, ...Field)
		ErrorfWithCtx(context.Context, string, ...interface{})
		FatalWithCtx(context.Context, string, ...Field)
		FatalfWithCtx(context.Context, string, ...interface{})
		Summary(tdr LogSummary)
		SummaryInfo(tdr LogSummary)
	}

	LogSummary struct {
		ExternalID     string      `json:"external_id"`
		JourneyID      string      `json:"journey_id"`
		ChainID        string      `json:"chain_id"`
		RespTime       int64       `json:"rt"`
		Error          string      `json:"error"`
		URI            string      `json:"uri"`
		Header         interface{} `json:"header"`
		Request        interface{} `json:"req"`
		Response       interface{} `json:"resp"`
		AdditionalData interface{} `json:"additional_data"`
	}

	Field struct {
		Key string
		Val interface{}
	}

	Level     string
	Formatter string

	Option struct {
		Level                       Level
		LogFilePath                 string
		Formatter                   Formatter
		MaxSize, MaxBackups, MaxAge int
		Compress                    bool
	}

	lumberjackHook struct {
		lbj    *lumberjack.Logger
		logrus *logrus.Logger
	}

	impl struct {
		instance *logrus.Logger
	}
)

const (
	Info  Level = "INFO"
	Debug Level = "DEBUG"
	Error Level = "ERROR"

	JSONFormatter Formatter = "JSON"
)

func (l *impl) Info(args ...interface{}) {
	l.instance.Info(args...)
}

func (l *impl) Infof(format string, args ...interface{}) {
	l.instance.Infof(format, args...)
}

func (l *impl) Debug(args ...interface{}) {
	l.instance.Debug(args...)
}

func (l *impl) Debugf(format string, args ...interface{}) {
	l.instance.Debugf(format, args...)
}

func (l *impl) Error(args ...interface{}) {
	l.instance.Error(args...)
}

func (l *impl) Errorf(format string, args ...interface{}) {
	l.instance.Errorf(format, args...)
}

func (l *impl) Warning(args ...interface{}) {
	l.instance.Warning(args...)
}

func (l *impl) Warningf(format string, args ...interface{}) {
	l.instance.Warningf(format, args...)
}

func (l *impl) Fatal(args ...interface{}) {
	l.instance.Fatal(args...)
}

func (l *impl) Fatalf(format string, args ...interface{}) {
	l.instance.Fatalf(format, args...)
}

func (l *impl) Print(args ...interface{}) {
	l.instance.Print(args...)
}

func (l *impl) Println(args ...interface{}) {
	l.instance.Println(args...)
}

func (l *impl) Printf(format string, args ...interface{}) {
	l.instance.Printf(format, args...)
}

func (l *impl) Instance() interface{} {
	return l.instance
}

func (l *impl) DebugWithCtx(ctx context.Context, message string, field ...Field) {
	l.instance.Debug(message, field)
}

func (l *impl) DebugfWithCtx(ctx context.Context, format string, args ...interface{}) {
	l.instance.Debugf(format, args...)
}

func (l *impl) InfoWithCtx(ctx context.Context, message string, field ...Field) {
	l.instance.Info(message, field)
}

func (l *impl) InfofWithCtx(ctx context.Context, format string, args ...interface{}) {
	l.instance.Infof(format, args...)
}

func (l *impl) WarnWithCtx(ctx context.Context, message string, field ...Field) {
	l.instance.Warning(message, field)
}

func (l *impl) WarnfWithCtx(ctx context.Context, format string, args ...interface{}) {
	l.instance.Warnf(format, args...)
}

func (l *impl) ErrorWithCtx(ctx context.Context, message string, field ...Field) {
	l.instance.Error(message, field)
}

func (l *impl) ErrorfWithCtx(ctx context.Context, format string, args ...interface{}) {
	l.instance.Errorf(format, args...)
}

func (l *impl) FatalWithCtx(ctx context.Context, message string, field ...Field) {
	l.instance.Fatal(message, field)
}

func (l *impl) FatalfWithCtx(ctx context.Context, format string, args ...interface{}) {
	l.instance.Fatalf(format, args...)
}

func (l *impl) Summary(tdr LogSummary) {
	l.instance.Info(tdr)
}

func (l *impl) SummaryInfo(tdr LogSummary) {
	l.instance.Info(tdr)
}

func New(option *Option) (Logger, error) {
	instance := logrus.New()

	if option.Level == Info {
		instance.Level = logrus.InfoLevel
	}

	if option.Level == Debug {
		instance.Level = logrus.DebugLevel
	}

	if option.Level == Error {
		instance.Level = logrus.ErrorLevel
	}

	var formatter logrus.Formatter

	if option.Formatter == JSONFormatter {
		formatter = &logrus.JSONFormatter{}
	} else {
		formatter = &logrus.TextFormatter{}
	}

	instance.Formatter = formatter

	// - check if log file path does exists
	if option.LogFilePath != "" {
		lbj := &lumberjack.Logger{
			Filename:   option.LogFilePath,
			MaxSize:    option.MaxSize,
			MaxAge:     option.MaxAge,
			MaxBackups: option.MaxBackups,
			LocalTime:  true,
			Compress:   option.Compress,
		}

		instance.Hooks.Add(&lumberjackHook{
			lbj:    lbj,
			logrus: instance,
		})
	}

	return &impl{instance}, nil
}

func (l *lumberjackHook) Levels() []logrus.Level {
	return []logrus.Level{logrus.InfoLevel, logrus.DebugLevel, logrus.ErrorLevel}
}

func (l *lumberjackHook) Fire(entry *logrus.Entry) error {
	b, err := l.logrus.Formatter.Format(entry)

	if err != nil {
		return err
	}

	if _, err := l.lbj.Write(b); err != nil {
		return err
	}

	return nil
}
