package middlewares

import (
	"bytes"
	"github.com/labstack/echo/v4"
	"gitlab.com/Chomsky49/signos-be/logger"
	ucontext "gitlab.com/Chomsky49/signos-be/pkg/context"
	uecho "gitlab.com/Chomsky49/signos-be/pkg/echo"
	pkgLogger "gitlab.com/Chomsky49/signos-be/pkg/logger"
	"gitlab.com/Chomsky49/signos-be/utilities"
	"io/ioutil"
	"strings"
)

const (
	ExternalID = "X-EXTERNAL-ID"
	JourneyID  = "X-JOURNEY-ID"
	ChainID    = "X-CHAIN-ID"
)

type (
	ContextInjectorMiddleware interface {
		Injector(next echo.HandlerFunc) echo.HandlerFunc
	}

	contextInjectorMiddleware struct {
		logger     logger.Logger
		prefixSkip []string
	}
)

func (i *contextInjectorMiddleware) Injector(h echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var (
			tid = c.Request().Header.Get(ExternalID)
			jid = c.Request().Header.Get(JourneyID)
			cid = c.Request().Header.Get(ChainID)
		)
		if len(tid) == 0 {
			tid, _ = utilities.NewUtils().GenerateUUID()
		}

		// - Set session to context
		ctx := ucontext.NewNUlfsaarContext(i.logger, tid, jid, cid)
		ctx.Context = c.Request().Context()
		ctx.Header = c.Request().Header
		ctx.URI = c.Request().URL.String()

		c.Set(uecho.Ulfsaar, ctx)

		// print request time
		var bodyBytes []byte
		if c.Request().Body != nil {
			bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
			// Restore the io.ReadCloser to its original state
			c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
		}
		ctx.Request = string(bodyBytes)
		if !i.skipper(c) {
			ctx.Info("Incoming",
				pkgLogger.ToField("url", c.Request().URL.String()),
				pkgLogger.ToField("header", ctx.Header),
				pkgLogger.ToField("request", ctx.Request))
		}

		return h(c)
	}
}

func (i *contextInjectorMiddleware) skipper(c echo.Context) (skip bool) {
	url := c.Request().URL.String()
	if url == "/" {
		skip = true
		return
	}

	for _, urlSkip := range i.prefixSkip {
		if strings.HasPrefix(url, urlSkip) {
			skip = true
			return
		}
	}

	return
}

func NewContextInjectorMiddleware(logger logger.Logger, prefixSkip ...string) (ContextInjectorMiddleware, error) {
	return &contextInjectorMiddleware{logger: logger, prefixSkip: prefixSkip}, nil
}
