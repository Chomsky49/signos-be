package echo

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/Chomsky49/signos-be/errors"
	"gitlab.com/Chomsky49/signos-be/pkg/context"
	"gitlab.com/Chomsky49/signos-be/pkg/logger"
	"gitlab.com/Chomsky49/signos-be/session"
	"gitlab.com/Chomsky49/signos-be/utilities"
	"net/http"
)

const (
	Ulfsaar = "UlfsaarContext"
	Session = "Session"
)

type (
	ApplicationContext struct {
		echo.Context
		Session        *session.Session
		UlfsaarContext *context.UlfsaarContext
	}

	Success struct {
		Code    string      `json:"code"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}

	Failed struct {
		Code    string      `json:"code"`
		Message string      `json:"message"`
		Error   string      `json:"error"`
		Data    interface{} `json:"data"`
	}
)

func (sc *ApplicationContext) Success(data interface{}) error {
	hc := http.StatusOK
	if data == nil {
		data = struct{}{}
	}

	res := Success{
		Code:    "00",
		Message: "success",
		Data:    data,
	}

	sc.UlfsaarContext.Response = res
	sc.UlfsaarContext.Info("Outgoing",
		logger.ToField("rt", sc.UlfsaarContext.ResponseTime()),
		logger.ToField("response", res),
		logger.ToField("http_code", hc))
	sc.UlfsaarContext.Summary()
	sc.UlfsaarContext.ErrorCode = res.Code
	sc.UlfsaarContext.ErrorMessage = res.Message

	return sc.JSON(hc, res)
}

func (sc *ApplicationContext) SuccessWithBodyMasking(parameterBody []string, data any) error {
	hc := http.StatusOK
	if data == nil {
		data = struct{}{}
	}

	maskDataBody := utilities.ConvertToFieldMask(parameterBody, data)

	res := Success{
		Code:    "00",
		Message: "success",
		Data:    maskDataBody,
	}

	sc.UlfsaarContext.Response = res
	sc.UlfsaarContext.Info("Outgoing",
		logger.ToField("rt", sc.UlfsaarContext.ResponseTime()),
		logger.ToField("response", res),
		logger.ToField("http_code", hc))
	sc.UlfsaarContext.Summary()
	sc.UlfsaarContext.ErrorCode = res.Code
	sc.UlfsaarContext.ErrorMessage = res.Message

	return sc.JSON(hc, res)
}

func (sc *ApplicationContext) SuccessWithAllMasking(parameterBody, parameterHeader []string, data any) error {
	hc := http.StatusOK
	if data == nil {
		data = struct{}{}
	}

	maskDataBody := utilities.ConvertToFieldMask(parameterBody, data)
	utilities.CheckFieldMaskHeader(parameterHeader, sc.UlfsaarContext)

	res := Success{
		Code:    "00",
		Message: "success",
		Data:    maskDataBody,
	}

	sc.UlfsaarContext.Response = res
	sc.UlfsaarContext.Info("Outgoing",
		logger.ToField("rt", sc.UlfsaarContext.ResponseTime()),
		logger.ToField("response", res),
		logger.ToField("http_code", hc))
	sc.UlfsaarContext.Summary()
	sc.UlfsaarContext.ErrorCode = res.Code
	sc.UlfsaarContext.ErrorMessage = res.Message

	return sc.JSON(hc, res)
}

func (sc *ApplicationContext) SuccessWithHeaderMasking(parameterHeader []string, data interface{}) error {
	hc := http.StatusOK
	if data == nil {
		data = struct{}{}
	}

	utilities.CheckFieldMaskHeader(parameterHeader, sc.UlfsaarContext)

	res := Success{
		Code:    "00",
		Message: "success",
		Data:    data,
	}

	sc.UlfsaarContext.Response = res
	sc.UlfsaarContext.Info("Outgoing",
		logger.ToField("rt", sc.UlfsaarContext.ResponseTime()),
		logger.ToField("response", res),
		logger.ToField("http_code", hc))
	sc.UlfsaarContext.Summary()
	sc.UlfsaarContext.ErrorCode = res.Code
	sc.UlfsaarContext.ErrorMessage = res.Message

	return sc.JSON(hc, res)
}

func (sc *ApplicationContext) SuccessWithMeta(data, meta interface{}) error {
	hc := http.StatusOK
	res := Success{
		Code:    "00",
		Message: "success",
		Data:    data,
	}

	sc.UlfsaarContext.Response = res
	sc.UlfsaarContext.Info("Outgoing",
		logger.ToField("rt", sc.UlfsaarContext.ResponseTime()),
		logger.ToField("response", res),
		logger.ToField("http_code", hc))
	sc.UlfsaarContext.Summary()
	sc.UlfsaarContext.ErrorCode = res.Code
	sc.UlfsaarContext.ErrorMessage = res.Message

	return sc.JSON(hc, res)
}

func (sc *ApplicationContext) Fail(err error) error {
	return sc.FailWithData(err, nil)
}

func (sc *ApplicationContext) FailWithData(err error, data interface{}) error {
	var (
		ed = errors.ExtractError(err)
	)

	if data == nil {
		data = struct{}{}
	}

	res := Failed{
		Code:    ed.Code,
		Message: ed.Message,
		Error:   ed.FullMessage,
		Data:    data,
	}

	sc.UlfsaarContext.Response = res
	sc.UlfsaarContext.Info("Outgoing",
		logger.ToField("rt", sc.UlfsaarContext.ResponseTime()),
		logger.ToField("response", res),
		logger.ToField("http_code", ed.HttpCode))
	sc.UlfsaarContext.Summary()
	sc.UlfsaarContext.ErrorCode = res.Code
	sc.UlfsaarContext.ErrorMessage = res.Message

	return sc.JSON(ed.HttpCode, res)
}

func (sc *ApplicationContext) Raw(hc int, data interface{}) error {
	if data == nil {
		data = struct{}{}
	}

	sc.UlfsaarContext.Response = data
	sc.UlfsaarContext.Info("Outgoing",
		logger.ToField("rt", sc.UlfsaarContext.ResponseTime()),
		logger.ToField("response", data),
		logger.ToField("http_code", hc))
	sc.UlfsaarContext.Summary()

	return sc.JSON(hc, data)
}

func (sc *ApplicationContext) AddSession(session *session.Session) *ApplicationContext {
	sc.Set(Session, session)
	sc.Session = session
	return sc
}

func (sc *ApplicationContext) AddUlfsaarContext(rc *context.UlfsaarContext) *ApplicationContext {
	sc.Set(Ulfsaar, rc)
	sc.UlfsaarContext = rc
	return sc
}

func ParseApplicationContext(c echo.Context) *ApplicationContext {
	var (
		nc   = c.Get(Ulfsaar)
		ss   = c.Get(Session)
		sess *session.Session
		ctx  *context.UlfsaarContext
	)

	// request context is mandatory on application context
	// force casting
	ctx = nc.(*context.UlfsaarContext)

	if ss != nil {
		sess, _ = ss.(*session.Session)
	}

	return &ApplicationContext{Context: c, UlfsaarContext: ctx, Session: sess}
}

func NewEmptyApplicationContext(parent echo.Context) *ApplicationContext {
	return &ApplicationContext{parent, nil, nil}
}

func NewApplicationContext(parent echo.Context) (*ApplicationContext, error) {
	pctx, ok := parent.(*ApplicationContext)
	if !ok {
		return nil, errors.ErrSession
	}
	if pctx.Session == nil {
		return nil, errors.ErrSession
	}
	return pctx, nil
}
