package http_client

import (
	"bytes"
	"crypto/tls"
	"github.com/go-resty/resty/v2"
	"gitlab.com/Chomsky49/signos-be/pkg/context"
	"gitlab.com/Chomsky49/signos-be/pkg/logger"
	"net/http"
	"time"
)

func Setup(options Options) HttpClient {
	httpClient := resty.New()

	if options.SkipTLS {
		httpClient.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	}

	if options.SkipCheckRedirect {
		httpClient.SetRedirectPolicy(resty.RedirectPolicyFunc(func(request *http.Request, requests []*http.Request) error {
			return http.ErrUseLastResponse
		}))
	}

	if options.WithProxy {
		httpClient.SetProxy(options.ProxyAddress)
	} else {
		httpClient.RemoveProxy()
	}

	httpClient.SetTimeout(options.Timeout)
	httpClient.SetDebug(options.DebugMode)

	return &client{
		options:    options,
		httpClient: httpClient,
	}
}

type (
	HttpClient interface {
		SetTimeout(timeout time.Duration)
		Post(ctx *context.UlfsaarContext, path string, headers http.Header, payload interface{}) (respHeader http.Header, statusCode int, body []byte, err error)
		PostFormData(ctx *context.UlfsaarContext, path string, headers http.Header, payload map[string]string) (respHeader http.Header, statusCode int, body []byte, err error)
		PostMultipartFormFilesAndData(ctx *context.UlfsaarContext, path string, headers http.Header, formData []*MultipartField, formFiles []MultipartFileRequest) (respHeader http.Header, statusCode int, body []byte, err error)
		Put(ctx *context.UlfsaarContext, path string, headers http.Header, payload interface{}) (respHeader http.Header, statusCode int, body []byte, err error)
		Get(ctx *context.UlfsaarContext, path string, headers http.Header) (respHeader http.Header, statusCode int, body []byte, err error)
		GetWithQueryParam(ctx *context.UlfsaarContext, path string, headers http.Header, queryParam map[string]string) (respHeader http.Header, statusCode int, body []byte, err error)
		Delete(ctx *context.UlfsaarContext, path string, headers http.Header, payload interface{}) (respHeader http.Header, statusCode int, body []byte, err error)
		Patch(ctx *context.UlfsaarContext, path string, headers http.Header, payload interface{}) (respHeader http.Header, statusCode int, body []byte, err error)
	}

	client struct {
		options    Options
		httpClient *resty.Client
	}
)

func (c *client) SetTimeout(timeout time.Duration) {
	c.httpClient.SetTimeout(timeout)
	return
}

func (c *client) Post(ctx *context.UlfsaarContext, path string, headers http.Header, payload interface{}) (respHeader http.Header, statusCode int, body []byte, err error) {
	url := c.options.Address + path
	startTime := time.Now()

	request := c.httpClient.R()
	request.SetBody(payload)

	for h, val := range headers {
		request.Header[h] = val
	}
	if headers[ContentType] == nil {
		request.Header.Set(ContentType, ApplicationJSON)
	}
	request.Header.Set(UserAgent, UserAgentValue)

	httpResp, httpErr := request.Post(url)

	if httpResp != nil {
		body = httpResp.Body()
		respHeader = httpResp.Header()
		statusCode = httpResp.StatusCode()
	}

	ctx.Info("Post",
		logger.ToField(urlKey, url),
		logger.ToField(requestKey, toRequest(request.Header, payload)),
		logger.ToField(responseKey, toResponse(statusCode, respHeader, body)),
		logger.ToField(startProcessingTimeKey, startProcessingTime(startTime)),
		logger.ToField(processingTimeKey, processingTime(startTime)),
	)

	if statusCode == http.StatusOK {
		return respHeader, statusCode, body, nil
	}

	return respHeader, statusCode, body, httpErr
}

func (c *client) PostFormData(ctx *context.UlfsaarContext, path string, headers http.Header, payload map[string]string) (respHeader http.Header, statusCode int, body []byte, err error) {
	url := c.options.Address + path
	startTime := time.Now()

	request := c.httpClient.R()
	request.SetFormData(payload)

	for h, val := range headers {
		request.Header[h] = val
	}
	if headers[ContentType] == nil {
		request.Header.Set(ContentType, ApplicationJSON)
	}
	request.Header.Set(UserAgent, UserAgentValue)

	httpResp, httpErr := request.Post(url)

	if httpResp != nil {
		body = httpResp.Body()
		respHeader = httpResp.Header()
		statusCode = httpResp.StatusCode()
	}

	ctx.Info("PostFormData",
		logger.ToField(urlKey, url),
		logger.ToField(requestKey, toRequest(request.Header, payload)),
		logger.ToField(responseKey, toResponse(statusCode, respHeader, body)),
		logger.ToField(startProcessingTimeKey, startProcessingTime(startTime)),
		logger.ToField(processingTimeKey, processingTime(startTime)),
	)

	if statusCode == http.StatusOK {
		return respHeader, statusCode, body, nil
	}

	return respHeader, statusCode, body, httpErr
}

func (c *client) PostMultipartFormFilesAndData(ctx *context.UlfsaarContext, path string, headers http.Header, formData []*MultipartField, formFiles []MultipartFileRequest) (respHeader http.Header, statusCode int, body []byte, err error) {
	url := c.options.Address + path
	startTime := time.Now()

	request := c.httpClient.R()
	request.SetMultipartFields(formData...)

	for _, val := range formFiles {
		request.SetFileReader(val.FieldName, val.FileName, bytes.NewReader(val.File))
	}

	for h, val := range headers {
		request.Header[h] = val
	}
	request.Header.Set(UserAgent, UserAgentValue)

	httpResp, httpErr := request.Post(url)

	if httpResp != nil {
		body = httpResp.Body()
		respHeader = httpResp.Header()
		statusCode = httpResp.StatusCode()
	}

	ctx.Info("PostMultipartFormFilesAndData",
		logger.ToField(urlKey, url),
		logger.ToField(requestKey, toRequest(request.Header, nil)),
		logger.ToField("formData", formData),
		logger.ToField("formFiles", formFiles),
		logger.ToField(responseKey, toResponse(statusCode, respHeader, body)),
		logger.ToField(startProcessingTimeKey, startProcessingTime(startTime)),
		logger.ToField(processingTimeKey, processingTime(startTime)),
	)

	if statusCode == http.StatusOK {
		return respHeader, statusCode, body, nil
	}

	return respHeader, statusCode, body, httpErr
}

func (c *client) Put(ctx *context.UlfsaarContext, path string, headers http.Header, payload interface{}) (respHeader http.Header, statusCode int, body []byte, err error) {
	url := c.options.Address + path
	startTime := time.Now()

	request := c.httpClient.R()

	for h, val := range headers {
		request.Header[h] = val
	}
	if headers[ContentType] == nil {
		request.Header.Set(ContentType, ApplicationJSON)
	}
	request.Header.Set(UserAgent, UserAgentValue)

	request.SetBody(payload)

	httpResp, httpErr := request.Put(url)

	if httpResp != nil {
		body = httpResp.Body()
		respHeader = httpResp.Header()
		statusCode = httpResp.StatusCode()
	}

	ctx.Info("Put",
		logger.ToField(urlKey, url),
		logger.ToField(requestKey, toRequest(request.Header, payload)),
		logger.ToField(responseKey, toResponse(statusCode, respHeader, body)),
		logger.ToField(startProcessingTimeKey, startProcessingTime(startTime)),
		logger.ToField(processingTimeKey, processingTime(startTime)),
	)

	if statusCode == http.StatusOK {
		return respHeader, statusCode, body, nil
	}

	return respHeader, statusCode, body, httpErr
}

func (c *client) Get(ctx *context.UlfsaarContext, path string, headers http.Header) (respHeader http.Header, statusCode int, body []byte, err error) {
	url := c.options.Address + path
	startTime := time.Now()

	request := c.httpClient.R()

	for h, val := range headers {
		request.Header[h] = val
	}
	request.Header.Set(UserAgent, UserAgentValue)

	httpResp, httpErr := request.Get(url)

	if httpResp != nil {
		body = httpResp.Body()
		respHeader = httpResp.Header()
		statusCode = httpResp.StatusCode()
	}

	ctx.Info("Get",
		logger.ToField(urlKey, url),
		logger.ToField(requestKey, toRequest(request.Header, nil)),
		logger.ToField(responseKey, toResponse(statusCode, respHeader, body)),
		logger.ToField(startProcessingTimeKey, startProcessingTime(startTime)),
		logger.ToField(processingTimeKey, processingTime(startTime)),
	)

	if statusCode == http.StatusOK {
		return respHeader, statusCode, body, nil
	}

	return respHeader, statusCode, body, httpErr
}

func (c *client) GetWithQueryParam(ctx *context.UlfsaarContext, path string, headers http.Header, queryParam map[string]string) (respHeader http.Header, statusCode int, body []byte, err error) {
	url := c.options.Address + path
	startTime := time.Now()

	request := c.httpClient.R()

	for h, val := range headers {
		request.Header[h] = val
	}
	request.Header.Set(UserAgent, UserAgentValue)
	request.SetQueryParams(queryParam)

	httpResp, httpErr := request.Get(url)

	if httpResp != nil {
		body = httpResp.Body()
		respHeader = httpResp.Header()
		statusCode = httpResp.StatusCode()
	}

	ctx.Info("GetWithQueryParam",
		logger.ToField(urlKey, url),
		logger.ToField(requestKey, toRequest(request.Header, queryParam)),
		logger.ToField(responseKey, toResponse(statusCode, respHeader, body)),
		logger.ToField(startProcessingTimeKey, startProcessingTime(startTime)),
		logger.ToField(processingTimeKey, processingTime(startTime)),
	)

	if statusCode == http.StatusOK {
		return respHeader, statusCode, body, nil
	}

	return respHeader, statusCode, body, httpErr
}

func (c *client) Delete(ctx *context.UlfsaarContext, path string, headers http.Header, payload interface{}) (respHeader http.Header, statusCode int, body []byte, err error) {
	url := c.options.Address + path
	startTime := time.Now()

	request := c.httpClient.R()

	for h, val := range headers {
		request.Header[h] = val
	}
	request.Header.Set(UserAgent, UserAgentValue)

	request.SetBody(payload)

	httpResp, httpErr := request.Delete(url)

	if httpResp != nil {
		body = httpResp.Body()
		respHeader = httpResp.Header()
		statusCode = httpResp.StatusCode()
	}

	ctx.Info("Delete",
		logger.ToField(urlKey, url),
		logger.ToField(requestKey, toRequest(request.Header, payload)),
		logger.ToField(responseKey, toResponse(statusCode, respHeader, body)),
		logger.ToField(startProcessingTimeKey, startProcessingTime(startTime)),
		logger.ToField(processingTimeKey, processingTime(startTime)),
	)

	if statusCode == http.StatusOK {
		return respHeader, statusCode, body, nil
	}

	return respHeader, statusCode, body, httpErr
}

func (c *client) Patch(ctx *context.UlfsaarContext, path string, headers http.Header, payload interface{}) (respHeader http.Header, statusCode int, body []byte, err error) {
	url := c.options.Address + path
	startTime := time.Now()

	request := c.httpClient.R()
	request.SetBody(payload)

	for h, val := range headers {
		request.Header[h] = val
	}
	if headers[ContentType] == nil {
		request.Header.Set(ContentType, ApplicationJSON)
	}
	request.Header.Set(UserAgent, UserAgentValue)

	httpResp, httpErr := request.Patch(url)

	if httpResp != nil {
		body = httpResp.Body()
		respHeader = httpResp.Header()
		statusCode = httpResp.StatusCode()
	}

	ctx.Info("Patch",
		logger.ToField(urlKey, url),
		logger.ToField(requestKey, toRequest(request.Header, payload)),
		logger.ToField(responseKey, toResponse(statusCode, respHeader, body)),
		logger.ToField(startProcessingTimeKey, startProcessingTime(startTime)),
		logger.ToField(processingTimeKey, processingTime(startTime)),
	)

	if statusCode == http.StatusOK {
		return respHeader, statusCode, body, nil
	}

	return respHeader, statusCode, body, httpErr
}
