package http_client

import (
	"encoding/json"
	"gitlab.com/Chomsky49/signos-be/pkg/logger"
	"time"
)

type (
	logRequest struct {
		Header interface{} `json:"header"`
		Body   interface{} `json:"body"`
	}
	logResponse struct {
		StatusCode int         `json:"status_code"`
		Header     interface{} `json:"header"`
		Body       interface{} `json:"body"`
	}
)

func startProcessingTime(start time.Time) string {
	return logger.ConvertLogTime(start.Format("2006-01-02 15:04:05.000"))
}

func processingTime(start time.Time) int64 {
	return time.Since(start).Milliseconds()
}

func toRequest(header, body interface{}) *logRequest {
	if body == nil {
		body = struct{}{}
	}
	return &logRequest{
		Header: header,
		Body:   body,
	}
}

func toResponse(statusCode int, header interface{}, body []byte) *logResponse {
	var data interface{}
	if body != nil {
		if _err := json.Unmarshal(body, &data); _err != nil {
			data = body
		}
	}
	return &logResponse{
		StatusCode: statusCode,
		Header:     header,
		Body:       data,
	}
}
