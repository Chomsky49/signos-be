package http_client

import "github.com/go-resty/resty/v2"

type MultipartFileRequest struct {
	FieldName string
	FileName  string
	File      []byte
}

type MultipartField = resty.MultipartField
