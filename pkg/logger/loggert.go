package logger

import (
	"context"
	"fmt"
	"gitlab.com/Chomsky49/signos-be/logger"
)

type (
	fileLogger struct {
		defaultLogger *zapLogger
	}

	Option struct {
		Stdout       bool   `json:"stdout"`
		FileLocation string `json:"file_location"`
		FileMaxAge   int    `json:"file_max_age"`
		Level        int8   `json:"level"`
	}
)

func NewLogger(config *Option) logger.Logger {
	fmt.Println("Try NewLogger File...")

	if config == nil {
		panic("logger file config is nil")
	}

	log := &fileLogger{
		defaultLogger: createLogger(config.Stdout, config.Level, config.FileLocation, config.FileMaxAge),
	}

	return log
}

func (c *fileLogger) Info(args ...interface{}) {
	c.InfoWithCtx(context.Background(), fmt.Sprint(args...))
}

func (c *fileLogger) Infof(format string, args ...interface{}) {
	c.InfofWithCtx(context.Background(), format, args...)
}

func (c *fileLogger) Debug(args ...interface{}) {
	c.DebugWithCtx(context.Background(), fmt.Sprint(args...))
}

func (c *fileLogger) Debugf(format string, args ...interface{}) {
	c.DebugfWithCtx(context.Background(), format, args...)
}

func (c *fileLogger) Error(args ...interface{}) {
	c.ErrorWithCtx(context.Background(), fmt.Sprint(args...))
}

func (c *fileLogger) Errorf(format string, args ...interface{}) {
	c.ErrorfWithCtx(context.Background(), format, args...)
}

func (c *fileLogger) Warning(args ...interface{}) {
	c.WarnWithCtx(context.Background(), fmt.Sprint(args...))
}

func (c *fileLogger) Warningf(format string, args ...interface{}) {
	c.WarnfWithCtx(context.Background(), format, args...)
}

func (c *fileLogger) Fatal(args ...interface{}) {
	c.FatalWithCtx(context.Background(), fmt.Sprint(args...))
}

func (c *fileLogger) Fatalf(format string, args ...interface{}) {
	c.FatalfWithCtx(context.Background(), format, args...)
}

func (c *fileLogger) Print(args ...interface{}) {
	c.defaultLogger.Print(args...)
}

func (c *fileLogger) Printf(format string, args ...interface{}) {
	c.defaultLogger.Printf(format, args...)
}

func (c *fileLogger) Println(args ...interface{}) {
	c.defaultLogger.Println(args...)
}

func (c *fileLogger) Instance() interface{} {
	return c.defaultLogger
}

func (c *fileLogger) DebugWithCtx(ctx context.Context, message string, fields ...logger.Field) {
	c.defaultLogger.Debug(ctx, message, fields...)
}

func (c *fileLogger) DebugfWithCtx(ctx context.Context, format string, args ...interface{}) {
	c.defaultLogger.Debugf(ctx, format, args...)
}

func (c *fileLogger) InfoWithCtx(ctx context.Context, message string, fields ...logger.Field) {
	c.defaultLogger.Info(ctx, message, fields...)
}

func (c *fileLogger) InfofWithCtx(ctx context.Context, format string, args ...interface{}) {
	c.defaultLogger.Infof(ctx, format, args...)
}

func (c *fileLogger) WarnWithCtx(ctx context.Context, message string, fields ...logger.Field) {
	c.defaultLogger.Warn(ctx, message, fields...)
}

func (c *fileLogger) WarnfWithCtx(ctx context.Context, format string, args ...interface{}) {
	c.defaultLogger.Warnf(ctx, format, args...)
}

func (c *fileLogger) ErrorWithCtx(ctx context.Context, message string, fields ...logger.Field) {
	c.defaultLogger.Error(ctx, message, fields...)
}

func (c *fileLogger) ErrorfWithCtx(ctx context.Context, format string, args ...interface{}) {
	c.defaultLogger.Errorf(ctx, format, args...)
}

func (c *fileLogger) FatalWithCtx(ctx context.Context, message string, fields ...logger.Field) {
	c.defaultLogger.Fatal(ctx, message, fields...)
}

func (c *fileLogger) FatalfWithCtx(ctx context.Context, format string, args ...interface{}) {
	c.defaultLogger.Fatalf(ctx, format, args...)
}

func (c *fileLogger) Summary(tdr logger.LogSummary) {
	c.defaultLogger.Summary(tdr)
}

func (c *fileLogger) SummaryInfo(tdr logger.LogSummary) {
	c.defaultLogger.SummaryInfo(tdr)
}
