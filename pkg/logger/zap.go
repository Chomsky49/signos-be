package logger

import (
	"context"
	"fmt"
	rotateLogs "github.com/lestrrat-go/file-rotatelogs"
	"gitlab.com/Chomsky49/signos-be/logger"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io"
	"os"
	"time"
)

const (
	logTimeKey        = "log_time"
	externalIDKey     = "external_id"
	journeyIDKey      = "journey_id"
	chainIDKey        = "chain_id"
	responseTimeKey   = "rt"
	responseURI       = "uri"
	headerKey         = "header"
	requestKey        = "req"
	responseKey       = "resp"
	errorKey          = "error"
	additionalDataKey = "additional_data"
	levelKey          = "level"
)

type logLevel string

const (
	debugLevel           = logLevel("Debug")
	infoLevel            = logLevel("Info")
	warnLevel            = logLevel("Warn")
	errorLevel           = logLevel("Error")
	panicLevel           = logLevel("Panic")
	fatalLevel           = logLevel("Fatal")
	summaryLevel         = logLevel("Summary")
	infoWithSummaryLevel = logLevel("InfoSummary")
)

type (
	ZapOption func(*zapLogger) error

	zapLogger struct {
		writers       []io.Writer
		closer        []io.Closer
		zapLog        *zap.Logger
		level         int8
		loggerChannel chan logItem
	}

	logItem struct {
		level   logLevel
		logTime time.Time
		ctx     context.Context
		message string
		args    []logger.Field
		summary logger.LogSummary
	}
)

func WithStdout() ZapOption {
	return func(logger *zapLogger) error {
		// Wire STD output for both type
		logger.writers = append(logger.writers, os.Stdout)
		return nil
	}
}

func WithFileOutput(location string, maxAge int) ZapOption {
	return func(logger *zapLogger) error {
		output, err := rotateLogs.New(
			location+".%Y%m%d",
			rotateLogs.WithLinkName(location),
			rotateLogs.WithMaxAge(time.Duration(maxAge)*24*time.Hour),
			rotateLogs.WithRotationTime(time.Hour),
		)

		if err != nil {
			return fmt.Errorf("sys file error: %w", err)
		}

		// Wire SYS config only in sys
		logger.writers = append(logger.writers, output)
		logger.closer = append(logger.closer, output)
		return nil
	}
}

// WithCustomWriter add custom writer, so you can write using any storage method
// without waiting this package to be updated.
func WithCustomWriter(writer io.WriteCloser) ZapOption {
	return func(logger *zapLogger) error {
		if writer == nil {
			return fmt.Errorf("writer is nil")
		}

		// wire custom writer to log
		logger.writers = append(logger.writers, writer)
		logger.closer = append(logger.closer, writer)
		return nil
	}
}

// WithLevel set level of logger
func WithLevel(level int8) ZapOption {
	return func(logger *zapLogger) error {
		logger.level = level
		return nil
	}
}

func NewZapLogger(opts ...ZapOption) (*zapLogger, error) {
	defaultLogger := &zapLogger{
		writers:       make([]io.Writer, 0),
		loggerChannel: make(chan logItem, 51200),
	}

	for _, o := range opts {
		if err := o(defaultLogger); err != nil {
			return nil, err
		}
	}

	// use stdout only when writer is not specified
	if len(defaultLogger.writers) <= 0 {
		defaultLogger.writers = append(defaultLogger.writers, zapcore.AddSync(os.Stdout))
	}

	encoderConfig := zapcore.EncoderConfig{
		TimeKey:    "time",
		MessageKey: "message",
		LineEnding: zapcore.DefaultLineEnding,
		EncodeDuration: func(d time.Duration, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendInt64(d.Nanoseconds() / 1000000)
		},
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(ConvertLogTime(t.Format("2006-01-02 15:04:05.999")))
		},
	}

	encoding := zapcore.NewJSONEncoder(encoderConfig)
	// set logger here instead in options to make easy and consistent initiation
	// set multiple writer as already set in options
	zapWriters := make([]zapcore.WriteSyncer, 0)
	for _, writer := range defaultLogger.writers {
		if writer == nil {
			continue
		}

		zapWriters = append(zapWriters, zapcore.AddSync(writer))
	}

	core := zapcore.NewCore(encoding, zapcore.NewMultiWriteSyncer(zapWriters...), zapcore.Level(defaultLogger.level))
	defaultLogger.zapLog = zap.New(core)

	// single thread for logging
	go func() {
		for {
			select {
			case log := <-defaultLogger.loggerChannel:
				defaultLogger.logByLevel(log.level, log.logTime, log.ctx, log.message, log.summary, log.args...)
			}
		}
	}()

	return defaultLogger, nil
}

func (d *zapLogger) Close() error {
	if d.closer == nil {
		return nil
	}

	var err error
	for _, closer := range d.closer {
		if closer == nil {
			continue
		}

		if e := closer.Close(); e != nil {
			err = fmt.Errorf("%w: %q", e, err)
		}
	}

	return err
}

func (d *zapLogger) Debug(ctx context.Context, message string, fields ...logger.Field) {
	d.loggerChannel <- logItem{level: debugLevel, logTime: time.Now(), ctx: ctx, message: message, args: fields}
}

func (d *zapLogger) Debugf(ctx context.Context, format string, args ...interface{}) {
	d.loggerChannel <- logItem{level: debugLevel, logTime: time.Now(), ctx: ctx, message: fmt.Sprintf(format, args...), args: []logger.Field{}}
}

func (d *zapLogger) Info(ctx context.Context, message string, fields ...logger.Field) {
	d.loggerChannel <- logItem{level: infoLevel, logTime: time.Now(), ctx: ctx, message: message, args: fields}
}

func (d *zapLogger) Infof(ctx context.Context, format string, args ...interface{}) {
	d.loggerChannel <- logItem{level: infoLevel, logTime: time.Now(), ctx: ctx, message: fmt.Sprintf(format, args...), args: []logger.Field{}}
}

func (d *zapLogger) Warn(ctx context.Context, message string, fields ...logger.Field) {
	d.loggerChannel <- logItem{level: warnLevel, logTime: time.Now(), ctx: ctx, message: message, args: fields}
}

func (d *zapLogger) Warnf(ctx context.Context, format string, args ...interface{}) {
	d.loggerChannel <- logItem{level: warnLevel, logTime: time.Now(), ctx: ctx, message: fmt.Sprintf(format, args...), args: []logger.Field{}}
}

func (d *zapLogger) Error(ctx context.Context, message string, fields ...logger.Field) {
	d.loggerChannel <- logItem{level: errorLevel, logTime: time.Now(), ctx: ctx, message: message, args: fields}
}

func (d *zapLogger) Errorf(ctx context.Context, format string, args ...interface{}) {
	d.loggerChannel <- logItem{level: errorLevel, logTime: time.Now(), ctx: ctx, message: fmt.Sprintf(format, args...), args: []logger.Field{}}
}

func (d *zapLogger) Fatal(ctx context.Context, message string, fields ...logger.Field) {
	d.loggerChannel <- logItem{level: fatalLevel, logTime: time.Now(), ctx: ctx, message: message, args: fields}
}

func (d *zapLogger) Fatalf(ctx context.Context, format string, args ...interface{}) {
	d.loggerChannel <- logItem{level: fatalLevel, logTime: time.Now(), ctx: ctx, message: fmt.Sprintf(format, args...), args: []logger.Field{}}
}

func (d *zapLogger) Print(args ...interface{}) {
	d.loggerChannel <- logItem{level: infoLevel, logTime: time.Now(), ctx: nil, message: fmt.Sprint(args...), args: []logger.Field{}}
}

func (d *zapLogger) Printf(format string, args ...interface{}) {
	d.loggerChannel <- logItem{level: infoLevel, logTime: time.Now(), ctx: nil, message: fmt.Sprintf(format, args...), args: []logger.Field{}}
}

func (d *zapLogger) Println(args ...interface{}) {
	d.loggerChannel <- logItem{level: infoLevel, logTime: time.Now(), ctx: nil, message: fmt.Sprint(args...), args: []logger.Field{}}
}

func (d *zapLogger) Summary(summary logger.LogSummary) {
	d.loggerChannel <- logItem{level: summaryLevel, logTime: time.Now(), ctx: nil, message: "Summary", args: nil, summary: summary}
}

func (d *zapLogger) SummaryInfo(summary logger.LogSummary) {
	d.loggerChannel <- logItem{level: infoWithSummaryLevel, logTime: time.Now(), ctx: nil, message: "Summary", args: nil, summary: summary}
}

func (d *zapLogger) logByLevel(level logLevel, logTime time.Time, ctx context.Context, message string, summary logger.LogSummary, fields ...logger.Field) {
	switch level {
	case debugLevel:
		zapLogs := []zap.Field{
			zap.String(levelKey, "debug"),
		}
		zapLogs = append(zapLogs, ctxToLog(ctx, logTime)...)
		zapLogs = append(zapLogs, appendToLog(fields...)...)

		d.zapLog.Debug(message, zapLogs...)
	case infoLevel:
		zapLogs := []zap.Field{
			zap.String(levelKey, "info"),
		}
		zapLogs = append(zapLogs, ctxToLog(ctx, logTime)...)
		zapLogs = append(zapLogs, appendToLog(fields...)...)
		d.zapLog.Info(message, zapLogs...)
	case warnLevel:
		zapLogs := []zap.Field{
			zap.String(levelKey, "warn"),
		}
		zapLogs = append(zapLogs, ctxToLog(ctx, logTime)...)
		zapLogs = append(zapLogs, appendToLog(fields...)...)
		d.zapLog.Warn(message, zapLogs...)
	case errorLevel:
		zapLogs := []zap.Field{
			zap.String(levelKey, "error"),
		}
		zapLogs = append(zapLogs, ctxToLog(ctx, logTime)...)
		zapLogs = append(zapLogs, appendToLog(fields...)...)
		d.zapLog.Error(message, zapLogs...)
	case panicLevel:
		zapLogs := []zap.Field{
			zap.String(levelKey, "panic"),
		}
		zapLogs = append(zapLogs, ctxToLog(ctx, logTime)...)
		zapLogs = append(zapLogs, appendToLog(fields...)...)
		d.zapLog.Panic(message, zapLogs...)
	case fatalLevel:
		zapLogs := []zap.Field{
			zap.String(levelKey, "fatal"),
		}
		zapLogs = append(zapLogs, ctxToLog(ctx, logTime)...)
		zapLogs = append(zapLogs, appendToLog(fields...)...)
		d.zapLog.Fatal(message, zapLogs...)
	case summaryLevel:
		convertLogTime := ConvertLogTime(logTime.Format("2006-01-02 15:04:05.000"))
		zapLogs := []zap.Field{
			zap.String(levelKey, "summary"),
			zap.String(logTimeKey, convertLogTime),
			zap.String(externalIDKey, summary.ExternalID),
			zap.String(journeyIDKey, summary.JourneyID),
			zap.String(chainIDKey, summary.ChainID),
			zap.Int64(responseTimeKey, summary.RespTime),
			zap.String(responseURI, summary.URI),
			formatLog(headerKey, summary.Header),
			formatLog(requestKey, summary.Request),
			formatLog(responseKey, summary.Response),
			zap.String(errorKey, summary.Error),
			formatLog(additionalDataKey, summary.AdditionalData),
		}
		d.zapLog.Info(message, zapLogs...)
	case infoWithSummaryLevel:
		convertLogTime := ConvertLogTime(logTime.Format("2006-01-02 15:04:05.000"))
		zapLogs := []zap.Field{
			zap.String(levelKey, "info"),
			zap.String(logTimeKey, convertLogTime),
			zap.String(externalIDKey, summary.ExternalID),
			zap.String(journeyIDKey, summary.JourneyID),
			zap.String(chainIDKey, summary.ChainID),
			zap.Int64(responseTimeKey, summary.RespTime),
			zap.String(responseURI, summary.URI),
			formatLog(headerKey, summary.Header),
			formatLog(requestKey, summary.Request),
			formatLog(responseKey, summary.Response),
			zap.String(errorKey, summary.Error),
			formatLog(additionalDataKey, summary.AdditionalData),
		}
		d.zapLog.Info(message, zapLogs...)
	}
}
