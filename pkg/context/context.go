package context

import (
	"context"
	"gitlab.com/Chomsky49/signos-be/logger"
	pkgLogger "gitlab.com/Chomsky49/signos-be/pkg/logger"
	"time"
)

type UlfsaarContext struct {
	Context        context.Context
	additionalData map[string]interface{}
	Logger         logger.Logger
	RequestTime    time.Time
	ExternalID     string
	JourneyID      string
	ChainID        string
	URI            string
	Header         interface{}
	Request        interface{}
	Response       interface{}
	ErrorCode      string
	ErrorMessage   string
}

func NewNUlfsaarContext(logger logger.Logger, xid, jid, cid string) *UlfsaarContext {
	return &UlfsaarContext{
		Context:        context.Background(),
		additionalData: map[string]interface{}{},
		Logger:         logger,
		RequestTime:    time.Now(),
		ExternalID:     xid,
		JourneyID:      jid,
		ChainID:        cid,
		Header:         map[string]interface{}{},
		Request:        struct{}{},
		Response:       struct{}{},
	}
}

func (c *UlfsaarContext) Get(key string) (data interface{}, ok bool) {
	data, ok = c.additionalData[key]
	return
}

func (c *UlfsaarContext) Put(key string, data interface{}) {
	c.additionalData[key] = data
}

func (c *UlfsaarContext) ToContextLogger() (ctx context.Context) {
	ctxVal := pkgLogger.Context{
		ExternalID:     c.ExternalID,
		JourneyID:      c.JourneyID,
		ChainID:        c.ChainID,
		AdditionalData: c.additionalData,
	}

	ctx = pkgLogger.InjectCtx(context.Background(), ctxVal)
	return
}

func (c *UlfsaarContext) Debug(message string, field ...logger.Field) {
	c.Logger.DebugWithCtx(c.ToContextLogger(), message, field...)
}

func (c *UlfsaarContext) Debugf(format string, arg ...interface{}) {
	c.Logger.DebugfWithCtx(c.ToContextLogger(), format, arg...)
}

func (c *UlfsaarContext) Info(message string, field ...logger.Field) {
	c.Logger.InfoWithCtx(c.ToContextLogger(), message, field...)
}

func (c *UlfsaarContext) Infof(format string, arg ...interface{}) {
	c.Logger.InfofWithCtx(c.ToContextLogger(), format, arg...)
}

func (c *UlfsaarContext) Warn(message string, field ...logger.Field) {
	c.Logger.WarnWithCtx(c.ToContextLogger(), message, field...)
}

func (c *UlfsaarContext) Warnf(format string, arg ...interface{}) {
	c.Logger.WarnfWithCtx(c.ToContextLogger(), format, arg...)
}

func (c *UlfsaarContext) Error(message string, field ...logger.Field) {
	c.Logger.ErrorWithCtx(c.ToContextLogger(), message, field...)
}

func (c *UlfsaarContext) Errorf(format string, arg ...interface{}) {
	c.Logger.ErrorfWithCtx(c.ToContextLogger(), format, arg...)
}

func (c *UlfsaarContext) Fatal(message string, field ...logger.Field) {
	c.Logger.FatalWithCtx(c.ToContextLogger(), message, field...)
}

func (c *UlfsaarContext) Fatalf(format string, arg ...interface{}) {
	c.Logger.FatalfWithCtx(c.ToContextLogger(), format, arg...)
}

func (c *UlfsaarContext) Summary() {
	model := logger.LogSummary{
		ExternalID:     c.ExternalID,
		JourneyID:      c.JourneyID,
		ChainID:        c.ChainID,
		RespTime:       c.ResponseTime(),
		Error:          c.ErrorMessage,
		URI:            c.URI,
		Header:         c.Header,
		Request:        c.Request,
		Response:       c.Response,
		AdditionalData: c.additionalData,
	}

	c.Logger.SummaryInfo(model)
	//c.Logger.Summary(model)
}

func (c *UlfsaarContext) ResponseTime() int64 {
	return time.Since(c.RequestTime).Milliseconds()
}

func (c *UlfsaarContext) GetAdditionalData() map[string]interface{} {
	return c.additionalData
}
